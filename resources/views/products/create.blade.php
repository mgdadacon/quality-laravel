@extends('layouts.master')
@section('header')
@stop
@section('content')

<h1>Create a product</h1>

<!-- if there are creation errors, they will show here -->
<!--{!! HTML::ul($errors->all()) !!} -->
@include ('errors.list')

{!! Form::open(['url' => 'products']) !!}
    
    @include ('products.form', ['submitButtonText' => 'Create product'])

{!! Form::close() !!}

@stop
