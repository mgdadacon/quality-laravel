@extends('layouts.master')
@section('header')
@stop
@section('content')

<h1>Edit {!! $product->name !!}</h1>

<!-- if there are creation errors, they will show here -->
<!-- {!! HTML::ul($errors->all()) !!} -->
@include ('errors.list')

{!! Form::model($product, array('route' => array('products.update', $product->id), 'method' => 'PUT')) !!}

    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', null, array('class' => 'form-control')) !!}
    </div>
	
	<div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::text('description', null, array('class' => 'form-control')) !!}
    </div>
	
	<div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::text('image', null, array('class' => 'form-control')) !!}
    </div>

    {!! Form::submit('Edit the Product', array('class' => 'btn btn-primary')) !!}
    {{-- @include ('partials.forms.form', ['submitButtonText' => 'Edit product']) --}}

{!! Form::close() !!}

@stop