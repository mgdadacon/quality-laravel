@extends('layouts.master')
@section('header')
@stop
@section('content')
<h2>All Products</h2>

@include ('errors.list')

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>Id</td>
            <td>Name</td>
            <td>Description</td>
            <td>Image</td>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
     {{-- @foreach($nerds as $key => $value) --}}
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td><img src="{{ $product->image }}" /></td>
            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                <!-- we will add this later since its a little more complicated than the other two buttons -->
{{--                {!! Form::open(array('url' => 'products/destroy' . $product->id, 'class' => 'pull-right'))!!}
                    {!! Form::hidden('id', $product->id) !!}
                    {!! Form::submit('Delete this Product', array('class' => 'btn btn-warning')) !!}
                {!! Form::close() !!} --}}

                <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('products/' . $product->id) }}">Show this product</a>

                <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('products/' . $product->id . '/edit') }}">Edit this product</a>



                <form method="post" action="http://localhost/quality/public/cart/addToCart" accept-charset="UTF-8">
                    {{-- <input name="_token" type="hidden" value="{{ csrf_token() }}"> --}}
                    
                <input name="product" type="hidden" value="{{$product->id}}"/> 

                {{-- value="{{ csrf_token() }} value="{{$product->id}}--}}
                
                    <p align="center"><button class="btn btn-info btnblock">Add to Cart</button></p>
                </form>



                {{-- <a class="btn btn-small btn-info" href="{{ URL::to('cart/' . $product->id . '/addItem') }}">Buy</a> --}}


                <div class="btn btn-small">
                    <div class="col-md-6 col-md-offset-3">
                        <a href="http://localhost/quality/public/cart/addProduct/{{$product->id}}" class="btn btn-success btn-product">
                            <span class="fa fa-shopping-cart"></span> 
                        Buy</a>
                    </div>
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@stop