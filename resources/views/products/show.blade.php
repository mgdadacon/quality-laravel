@extends('layouts.master')
@section('header')
@stop
@section('content')

<h1>Showing {{ $product->name }}</h1>

    <div class="jumbotron text-center">
        <h2>{{ $product->name }}</h2>
		<p>
            <strong>Description:</strong> {{ $product->description }}<br>
            <strong>Image</strong> <img src="{{ asset($product->image) }}" />
        </p>
    </div>

    <div class="row">
	    <div class="col-md-6">
	        <a href="{{ route('products.index') }}" class="btn btn-info">Back to all Products</a>
	        <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Edit Product</a>
	    </div>

	    <div class="pull-right">
	        {!! Form::open(['method' => 'DELETE', 'route' => ['products.destroy', $product->id]]) !!}
	            {!! Form::submit('Delete this product', ['class' => 'btn btn-danger']) !!}
	        {!! Form::close() !!}
    	</div>
    </div>
    
@stop