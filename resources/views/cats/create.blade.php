@extends('layouts.catsmaster')
{{-- @section('header') --}}
{{-- @stop --}}
@section('content')
<h1>Create Cat</h1>
{!! Form::open(['url' => 'cats']) !!} {{--post to cats not cats/create --}}

	<div class="form-group">
		{!! Form::label('name', 'Name') !!}
		{!! Form::text('name', null, ['class'=> 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('description', 'Description') !!}
		{!! Form::text('description', null, ['class'=> 'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Add Cat', ['class' => 'btn btn-primary form-control']) !!}
	</div>

{!! Form::close() !!}

@if ($errors->any())
	<ul class="alert alert-danger">
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>
@endif

@stop