@extends('layouts.catsmaster')
{{-- @section('header') --}}
{{-- @stop --}}
@section('content')
<h1>Cats</h1>
<hr/>
@foreach ($mgaPusa as $cat)
<cat>
	<h2>
		<a href="{{ url('/cats', $cat->id) }}">{{ $cat->name}}</a>
	</h2>

	<div class="body">{{ $cat->description}}</div>

	<cat>

@endforeach
@stop