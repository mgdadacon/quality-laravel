<nav class="navbar navbar-inverse">
	@if (Auth::check())
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ URL::to('about') }}">About Quality Clothing</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('products') }}">View All products</a></li>
				<li><a href="{{ URL::to('products/create') }}">Create products</a>
				<li><a href="{{ URL::to('auth/logout') }}">Logout</a>
			</ul>
		@endif

	@if (Auth::guest())
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ URL::to('about') }}">About Quality Clothing</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('auth/login') }}">Login</a>
				<li><a href="{{ url('/auth/register') }}">Register</a></li>
			</ul>
		</nav>
	@endif