
    <div class="form-group">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('description', 'Description') !!}
        {!! Form::text('description', Input::old('description'), ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
        {!! Form::label('image', 'Image') !!}
        {!! Form::text('image', Input::old('image'), ['class' => 'form-control']) !!}
    </div>

    {{-- {!! Form::submit('Create product', array('class' => 'btn btn-primary')) !!} --}}

    {{-- <div class="form-group"> --}}
        {{-- {!! Form::submit($submitButtonText,  ['class' => 'btn btn-primary']) !!} --}}
    {{-- </div> --}}

    {!! Form::submit('Create product', ['class' => 'btn btn-primary']) !!}
