<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CatsLaravel MVC</title>
	<!--<link rel="stylesheet" href="{{asset('bootstrap-3.0.0.min.css')}}">-->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<div class="header">
		<nav class="navbar navbar-inverse">
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ URL::to('cats') }}">Cats Store</a>
			</div>
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::to('cats') }}">View All Cats</a></li>
				<li><a href="{{ URL::to('cats/create') }}">Create Cat</a>
			</ul>
		</nav>
		</div>
		@yield('header')
		
<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
		@yield('content')
	</div>
</body>