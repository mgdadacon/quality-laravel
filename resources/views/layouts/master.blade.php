<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Products Laravel MVC</title>
	<!--<link rel="stylesheet" href="{{asset('bootstrap-3.0.0.min.css')}}">-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	{{-- <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"> --}}
</head>
<body>
	<div class="container">
		<div class="header">

		@include ('partials.navbar')

		</div>
		@yield('header')
		
<!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
		@yield('content')
<!-- if there are creation errors, they will show here -->
{{-- {!! HTML::ul($errors->all()) !!} --}}
	</div>
</body>