<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder {
	public function run() {
		DB::table('products')->insert([
		['id' => 1, 'name' => "Wool", 'description' => "Its in the miles", 'image' => "resources/assets/wool.jpg"],
		['id' => 2, 'name' => "Party", 'description' => "Happy party", 'image' => "resources/assets/party.jpg"],

		]);
	}
}