<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function() 
{
	return redirect('about');
});

Route::get('about', 'PagesController@about');

// Route::get('about', function() {
// return view('about')->with('number_of_products', 998);
// });

// Admin area
get('products', function () {
  return redirect('/products/index');
});
$router->group([
  // 'namespace' => 'Admin',
  'middleware' => 'auth',
], function () {
  resource('products', 'ProductController');
});


// Route::resource('products', 'ProductController');

//
//Route::get('products/create', 'ProductController@create');

Route::get('cats', 'CatController@index');

// Route::get('cats/create', 'CatController@create');

// Route::get('cats/{id}', 'CatController@show');

// Route::post('cats', 'CatController@store');//post save new cat





// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


Route::controllers([
   'password' => 'Auth\PasswordController',
]);


// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);


// Route::get('/cart', 'CartController@getIndex');

// Route::post('/cart/addToCart', 'CartController@addToCart');

// Route::get('/cart/delete/{id}', 'CartController@getDelete');


// Route::resource('cart', 'CartController');

// Route::get('/cart', function() {
// return redirect('cart');
// });

// Route::get('cart', 'CartController@showCart');

// Route::get('addProduct/{productId}', 'CartController@addItem');

// Route::get('removeItem/{productId}', 'CartController@removeItem');



/* Route::get('/', function() {
return 'All cats';
});

Route::get('cats/{id}', function($id) {
return sprintf('Cat #%s', $id);
});

Route::get('cats/{id}', function($id) {
sprintf('Cat #%d', $id);
})->where('id', '[0-9]+');

Route::get('about', function() {
return view('about')->with('number_of_cats', 9000);
});

Route::get('cats', function() {
$cats = App\Cat::all();
return view('cats.index')->with('cats', $cats);
});

Route::get('cats/breeds/{name}', function($name) {
$breed = App\Breed::with('cats')
->whereName($name)
->first();
return view('cats.index')
->with('breed', $breed)
->with('cats', $breed->cats);
}); */

/* Route::get('cats/{id}', function($id) {
$cat = App\Cat::find($id);
return view('cats.show) ->with('cat', $cat);
}); */

/* Route::get('cats/{cat}', function(App\Cat $cat) {
return view('cats.show')->with('cat', $cat);
}); */
