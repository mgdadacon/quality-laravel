<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Cat;

//use Carbon\Carbon; //jeff, timestamp, updated_at
// use Input; // works also
// use Request; //jeff
use Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CatController extends Controller
{


    public function index()
    {
    	$cats = Cat::all();

        //$cats = Cat::latest()->get();//latest orderdesecending needs created_at column timestamp

    	// return view('cats.index', compact('cats'));
    	return view('cats.index')->with('mgaPusa', $cats); //'mgaPusa' variable avaialable for view
    }


    public function show($id)
    {
    	$cat = Cat::find($id);

    	// return view('cats.show', compact('cat'));
        return view('cats.show')->with('pusa', $cat);//pusa is $pusa variable in view
    }


    public function create()
    {
        return view('cats.create');
    }


    public function store(Request $request)//jeff with validation
    // public function store()
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required']);//jeff with validation
        // Cat::create($request->all());//jeff with validation


        // $input = Request::all();//jeff, fetch the input, can be get('name') example
        
        //$input['created_at'] = Carbon::now();//jeff timestamp, updated_at

        // Cat::create($input);//jeff

        

        $request = Input::all();//fetch the input
        $cat = new Cat;
        $cat->name = $request['name'];
        $cat->description = $request['description'];
        $cat->save();


        // $input = Input::all();
        // $cat = new Cat;
        // $cat->name = $input['name'];
        // $cat->description = $input['description'];
        // $cat->save();

        // $cat = new Cat;
        // $cat->name = Input::get('name');
        // $cat->description = Input::get('description');
        // $cat->save();


        // $cat = new Cat;
        // $cat->name = $request->input('name');
        // $cat->description = $request->input('description');
        // $cat->save();

        Session::flash('message', 'Successfully created cat!');
        return redirect('cats');

        // return $input;
    }


}
