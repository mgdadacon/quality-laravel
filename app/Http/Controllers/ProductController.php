<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use Session;
use View;
use Auth;

use App\Product;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return \Auth::user();

        $products = Product::all();

        // $products = Product::latest('published_at')->get();

        // load the view and pass the nerds
        return View::make('products.index')->with('products', $products);
		/* return $products; */
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        return View::make('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules); 
        // $validator = Validator::make(Input::all(), Product::$rules); //validator in Model

        // process the login
        if ($validator->fails()) {
            return Redirect::to('products/create')
                ->withErrors($validator) //display errors in View via {!! HTML::ul($errors->all()) !!}
                ->withInput(Input::except('password')); //remember old input from form
        } else {
            // store
            $product = new Product;
            $product->name = Input::get('name');
			$product->description = Input::get('description');
			$product->image = Input::get('image');
            $product->save();

            // redirect
            // Session::flash('message', 'Successfully created product!');
            // return Redirect::to('products');

            return Redirect::to('products')->with('message', 'Successfully created product!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);		
        return View::make('products.show')->with('product', $product); //why single product not products?
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return View::make('products.edit')->with('product', $product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
			'description' => 'required',
			'image' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('products/' . $id . '/edit')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $product = Product::find($id);
            $product->name = Input::get('name');
			$product->description = Input::get('description');
			$product->image = Input::get('image');
            $product->save();

            // redirect
            Session::flash('message', 'Successfully updated product!');
            return Redirect::to('products');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    // public function destroy()
    {
            // $product = Product::find(Input::get($id));
            $product = Product::find($id);
            
            if($product)
            {
                $product->delete();
                return Redirect::to('products')->with('message', 'Product Deleted');
            }

            return Redirect::to('products')->with('message', 'Something went wrong, cannot delete product');
    }
}
