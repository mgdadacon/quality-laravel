<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;

use View;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CartController extends Controller
{


    public function addToCart()
    {
        $rules=array('product'=>'required|numeric|exists:products,id');

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails())
        {
            // return Redirect::route('products')->with('error','The book could not added to your cart!');
            return Redirect::to('products')
            ->withErrors($validator); //display errors in View via {!! HTML::ul($errors->all()) !!}
        }

        
        $product_id = Input::get('product');
        $product = Product::find($product_id); //Product maybe new Product?

        // $count = Cart::where('product_id','=',$product_id)->count();

        // if($count)
        // {
        //     return Redirect::route('index')->with('error','The book already in your cart.');
        // }

        Cart::create(array('product_id'=>$product_id));

        // return Redirect::route('cart');
        return Redirect::to('cart');
    }


    public function getIndex()
    {        
        $cart_products=Cart::with('Product')->get();

        if(!$cart_products){
            return Redirect::route('index')->with('error','Your cart is empty');
        }
            return View::make('cart')
        ->with('cart_products', $cart_products);

        // return 'getIndex cart';
    }


    public function getDelete($id)
    {
        $cart = Cart::find($id)->delete();
        return Redirect::route('cart');
    }

}
