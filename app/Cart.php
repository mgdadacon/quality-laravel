<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    // public function user()
    // {
    //     return $this->belongsTo('App\User');
    // }

    protected $fillable = ['product_id'];

    public function Product(){
		return $this->belongsTo('App\Product','product_id');
	}
 
    // public function cartItems()
    // {
    //     return $this->hasMany('App\CartItem');
    // }
}
