<?php namespace App;

use Illuminate\Database\Eloquent\Model;


	class Cat extends Model {

		//public $timestamps = false;
		
		protected $fillable = ['name','description'];
		
		// public function breed() {
		// 	return $this->belongsTo('App\Breed');
		// }
}